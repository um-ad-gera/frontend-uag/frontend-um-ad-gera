const path = require("path");
const withImages = require('next-images')

module.exports = withImages({
  env: {
    REACT_APP_DESIGN_MODE: process.env.REACT_APP_DESIGN_MODE,
    API_REMOTE_BASE_URL: process.env.API_REMOTE_BASE_PROTOCOL+"://"+process.env.API_REMOTE_BASE_URL,
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  images: {
    domains: [process.env.API_REMOTE_BASE_URL],
  },
  i18n: {
    locales: ['is','en'],
    defaultLocale: 'is',
    localeDetection: false,
  },
});