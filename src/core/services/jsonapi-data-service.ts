import _, { merge } from "lodash";
import URI from "urijs";
import Kitsu from "kitsu";
import { UrlBuilder } from "../utils/url/url-builder";
import { PageProps } from "../../pages/[[...slug]]";
import { SplitTitleType } from "../../components/cards/title-description-card/card-title";

export const JsonApiData = (function () {
  return {
    getAllMenuItems(
      skipRoot: boolean = false,
      locale: string,
      defaultLocale: string
    ) {
      return _getAllMenuItems(skipRoot, locale, defaultLocale);
    },
    /* getAllChildMenuItems(slug) {
      return _getAllChildMenuItems(slug);
    }, */
    getPageData(slug: string, locale: string, defaultLocale: string) {
      return _getPageData(slug, locale, defaultLocale);
    },

    getAllRoutes(locales: string[], defaultLocale: string) {
      return _getAllRoutes(locales, defaultLocale);
    },
    getEntityParagraphsData(
      slug: string,
      locale: string,
      defaultLocale: string
    ) {
      return _getEntityParagraphsData(slug, locale, defaultLocale);
    },

    utils: {
      toSplitHeadline: (headlineArr: string[]) => {
        return _toSplitHeadline(headlineArr);
      },
      getImgSrcFromImageField: (image?: {
        data?: { field_media_image?: { data?: { uri?: { url?: string } } } };
      }) => {
        return _getImgSrcFromImageField(image);
      },
      getUrlWithoutRoot: (url: string) => {
        return _getUrlWithoutRoot(url);
      },
    },
  };

  /**
   * menu functions
   */

  type MenuItem = {
    id: string;
    parentId?: string;
    title: string;
    url: string;
    children?: MenuItem[];
  };

  async function _getAllMenuItems(
    skipRoot: boolean = false,
    locale: string,
    defaultLocale: string
  ) {
    const uri = new URI(process.env.API_REMOTE_BASE_URL)
      .segment(_getLocaleSegment(locale, defaultLocale))
      .segment("jsonapi/menu_items");

    const api = new Kitsu({
      baseURL: uri.valueOf(),
      pluralize: false,
    });

    const kitsuData = await api.get(`umadgera-is`, {
      headers: {},
      params: {
        include: "",
        fields: "",
        sort: "",
        filter: "",
        page: {
          limit: 0,
          after: "",
          before: "",
          number: 0,
          offset: 0,
          size: 0,
        },
      },
    });

    const allMenuItems = kitsuData?.data;
    const newMenuItems = allMenuItems
      ?.filter((menuItem: any) => !menuItem?.parent)
      .map((menuItem: any) => {
        let urlWithoutRoot = _getUrlWithoutRoot(menuItem?.url);
        const urlIsRoot = urlWithoutRoot === "";
        urlWithoutRoot = urlIsRoot ? "/" : urlWithoutRoot;

        const mi: MenuItem = {
          id: menuItem.id,
          title: menuItem.title,
          url: urlWithoutRoot,
          children: _getChildMenuItems(menuItem.id, allMenuItems),
        };

        return mi;
      });

    return !skipRoot ? newMenuItems : newMenuItems[0]?.children;
  }

  function _getChildMenuItems(
    parentId: string,
    allMenuItems: any[]
  ): MenuItem[] {
    if (!allMenuItems) return;

    const childMenuItems = allMenuItems
      ?.filter((item) => item.parent === parentId)
      ?.map((item) => {
        const urlWithoutRoot = _getUrlWithoutRoot(item?.url);

        const menuItem: MenuItem = {
          id: item.id,
          title: item.title,
          url: urlWithoutRoot,
          parentId: parentId,
          children: _getChildMenuItems(item.id, allMenuItems),
        };

        return menuItem;
      });

    return childMenuItems;
  }

  async function _getAllChildMenuItems(slug) {
    let res = await _resolvePageSlugToResourceUrl(slug, "", "");
    const allMenuItems = await _getAllMenuItems(false, "", "");

    const parentNode = allMenuItems.data.find((item) => {
      return item.route.parameters.node === res.entity.id;
    });

    const childMenuItems = allMenuItems.data.filter((item) => {
      return item.parent === parentNode.id;
    });
    return childMenuItems;
  }

  async function _getAllRoutes(locales: string[], defaultLocale: string) {
    let allRoutes: any[] = [];

    merge(
      allRoutes,
      locales.forEach((locale) => {
        return _getLocaleRoutes(locale, defaultLocale);
      })
    );

    return allRoutes;
  }

  /**
   * data functions
   */
  async function _getPageData(
    slug: string,
    locale: string,
    defaultLocale: string
  ): Promise<PageProps> {
    let res = await _resolvePageSlugToResourceUrl(slug, locale, defaultLocale);

    if (!res?.entity?.type) {
      return;
    }

    const baseuri = new URI(process.env.API_REMOTE_BASE_URL).segment([
      _getLocaleSegment(locale, defaultLocale),
      "jsonapi",
    ]);

    const baseuriWithType = new URI(baseuri);

    /* get page data */
    const api = new Kitsu({
      baseURL: baseuri.valueOf(),
      pluralize: false,
    });

    const kitsuData = await api.get(
      `${res?.entity?.type}/${res.entity.bundle}/${res.entity.uuid}`
    );

    const hasHeader = _pageHasHeader(kitsuData.data);
    const hasContentParagraphs = _pageHasContentParagraphs(kitsuData.data);

    /** TODO: find a better way to get the footer blocks... */
    const kitsuFooterData = await api.get(
      `block_content/text_block` /* , {
      params: {
        filter: {
          idgroup: { group: { conjunction: "OR" } },
          idfilter1: {
            condition: {
              path: "id",
              value: "ad2549ea-ef68-43fc-b936-aace0b9365f9",
              memberOf: "idgroup",
            },
          },
          idfilter2: {
            condition: {
              path: "id",
              value: "53239f80-7580-44c3-af16-200838c26c91",
              memberOf: "idgroup",
            },
          },
        },
        fields: {},
        include: "",
        page: {
          after: "",
          before: "",
          limit: 0,
          size: 0,
          number: 0,
          offset: 0,
        },
        sort: [""],
      },
      headers: {}
    } */
    );

    /** TODO: find a better way to sort!
     *  jsonapi sort brings errors to the table although done as instructed
     *  Might have something to do with the filter grouping above....
     */
    const sortedFooterData = /* _.reverse( */ kitsuFooterData?.data; /* ) */

    return {
      data: kitsuData.data,
      footer: sortedFooterData,
      hasHeader,
      hasContentParagraphs,
      menuItems: [],
      contactUsTitle: _.get(_.last(sortedFooterData), "info"),
    };
  }

  async function _getEntityParagraphsData(
    slug: string,
    locale: string,
    defaultLocale: string
  ) {
    let res = await _resolvePageSlugToResourceUrl(slug, locale, defaultLocale);

    const uri = new URI(process.env.API_REMOTE_BASE_URL).segment([
      _getLocaleSegment(locale, defaultLocale),
      "jsonapi",
      res?.entity?.type,
    ]);

    const api = new Kitsu({
      baseURL: uri.valueOf(),
      pluralize: false,
    });

    const bundleOfType = _getBundleOfType(res);

    let includesArr = [];
    /**
     * @var
     * @type {{type: string, values: string[]}[]}
     */
    let fieldsArr = [];

    switch (bundleOfType) {
      case "node--employee":
        includesArr.push({
          referenceField: "field_content_paragraphs",
          nestedFields: [
            "field_image",
            "field_image.field_media_image",
            "field_file.field_media_file",
          ],
        });

        fieldsArr.push({
          type: bundleOfType,
          values: ["field_content_paragraphs"],
        });
        break;
      case "node--umadgera_is_page_project":
      case "node--umadgera_is_page":
        includesArr.push({
          referenceField: "field_content_paragraphs",
          nestedFields: [
            "field_page_reference.field_preview_image",
            "field_page_reference.field_preview_image.field_media_image",
            "field_image.field_media_image",
            "field_file.field_media_file",
          ],
        });

        fieldsArr.push({
          type: bundleOfType,
          values: [
            "field_preview_image",
            "field_preview_text",
            "field_split_headline",
          ],
        });
        break;
      default:
        break;
    }

    const includes = UrlBuilder.getIncludesString({
      includes: includesArr,
    });
    const fields = UrlBuilder.getFieldsString({
      fields: fieldsArr,
    });

    const kitsuData = await api.get(`${res.entity.bundle}/${res.entity.uuid}`, {
      params: {
        include: includes,
        fields: "",
        filter: "",
        sort: "",
        page: {
          after: "",
          before: "",
          limit: 0,
          size: 0,
          number: 0,
          offset: 0,
        },
      },
      headers: {},
    });

    return kitsuData?.data ?? {};
  }

  async function _getAllEmployeesData(slug: any) {
    /* let res = await _resolvePageSlugToResourceUrl(slug); */
    const api = new Kitsu({
      /* baseURL: `${res.jsonapi.entryPoint}`, */
      baseURL: `${process.env.API_REMOTE_BASE_URL}/api/jsonapi`,
      pluralize: false,
    });

    let includesArr = [];
    /**
     * @var
     * @type {{type: string, values: string[]}[]}
     */
    let fieldsArr = [];

    includesArr.push({
      referenceField: "field_content_paragraphs",
      nestedFields: [
        "field_image",
        "field_image.field_media_image",
        "field_file.field_media_file",
      ],
    });

    fieldsArr.push({
      type: "node--employee",
      values: ["field_content_paragraphs", "title"],
    });

    const includes = UrlBuilder.getIncludesString({
      includes: includesArr,
    });
    const fields = UrlBuilder.getFieldsString({
      fields: fieldsArr,
    });

    let kitsuData = await api.get(
      `node/employee` /* , {
      params: {
        include: includes,
        fields: fields,
      },
    } */
    );

    return kitsuData?.data ?? [];
  }

  /**
   * utils
   */

  function _toSplitHeadline(headlineArr: string[]): SplitTitleType {
    let first;
    let second;

    if (!headlineArr) {
      return;
    }
    if (headlineArr.length > 0) {
      first = headlineArr[0];
    }
    if (headlineArr.length > 1) {
      second = headlineArr[1];
    }

    const splitHeadline: SplitTitleType = {
      first: headlineArr[0],
      [second ? "second" : ""]: second ? second : "",
    };
    return splitHeadline;
  }

  function _getImgSrcFromImageField(image: {
    data?: { field_media_image?: { data?: { uri?: { url?: string } } } };
  }): string {
    return image?.data?.field_media_image?.data?.uri?.url;
  }

  function _getUrlWithoutRoot(url: string) {
    //todo: replace hardcoded string with env var
    return (
      url
        ?.replace(/^(\/forsida)/, "")
        .replace(/^(\/en\/frontpage)/, "/en")
        .replace(/^(\/frontpage)/, "") ?? "/"
    );
  }

  /**
   * helpers
   */
  function _getBundleOfType(res: { entity: { type: string; bundle: string } }) {
    return `${res.entity.type}--${res.entity.bundle}`;
  }

  async function _resolvePageSlugToResourceUrl(
    slug: string,
    locale: string,
    defaultLocale: string
  ) {
    var apiSlug = _frontendSlugToApiSlug(slug, locale, defaultLocale);
    const uri = new URI(process.env.API_REMOTE_BASE_URL)
      .segment(_getLocaleSegment(locale, defaultLocale))
      .segment("router/translate-path")
      .addQuery("path", apiSlug);

    const paths = await fetch(
      decodeURI(uri.valueOf().replace(/%3F/g, "?").replace(/%2F/g, "/"))
    );

    return await paths.json();
  }

  function _frontendSlugToApiSlug(
    slug: string,
    locale: string,
    defaultLocale: string
  ) {
    let result = "";
    let menuRoot = process.env.API_MENU_ROOT;

    const localeSegment = _getLocaleSegment(locale, defaultLocale);

    switch (localeSegment) {
      case "en":
        menuRoot = process.env.API_MENU_ROOT_LOCALE_EN;
        break;
    }

    if (slug.length === 0) {
      result = [localeSegment, menuRoot].join("/");
    } else {
      result = [localeSegment, menuRoot, slug].join("/");
    }

    return result;
  }

  function _getLocaleSegment(locale: string, defaultLocale: string) {
    return locale !== defaultLocale ? locale : "";
  }
  async function _getLocaleRoutes(
    locale: string,
    defaultLocale: string
  ): Promise<void> {
    const uri = new URI(process.env.API_REMOTE_BASE_URL).segment([
      _getLocaleSegment(locale, defaultLocale),
      "jsonapi/menu_items",
    ]);

    const api = new Kitsu({
      baseURL: uri.valueOf(),
      pluralize: false,
    });

    const kitsuData = await api.get(`umadgera-is`, {
      headers: {},
      params: {
        include: "",
        fields: "",
        sort: "",
        filter: "",
        page: {
          limit: 0,
          after: "",
          before: "",
          number: 0,
          offset: 0,
          size: 0,
        },
      },
    });

    const localeRoutes = kitsuData?.data
      ?.filter((menuLink: { url: string }) => {
        //filter out all menu items created with <nolink>
        return menuLink?.url;
      })
      .map((menuLink: { url: string }) => {
        const urlWithoutRoot = _getUrlWithoutRoot(menuLink?.url);
        return {
          params: { slug: urlWithoutRoot?.replace(/^\/+/g, "").split("/") },
        };
      });
    return localeRoutes;
  }
})();
function _pageHasHeader(data: any) {
  return (
    data?.field_header_headline?.length > 0 ||
    data?.field_header_text !== null ||
    data?.field_header_image?.data !== null
  );
}
function _pageHasFooter(data: any) {
  return true;
}

function _pageHasContentParagraphs(data: any) {
  return data?.field_content_paragraphs?.data?.length > 0;
}
