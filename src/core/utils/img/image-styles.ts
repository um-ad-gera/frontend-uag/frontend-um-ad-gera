import _ from "lodash";
import { ImageStyleURI } from "../../models/api/image-styles";

export type imgType = {
  type: string;
};
export type imgTypeSizeDefinitionType = {
  size: string;
  media: string;
} & imgType;
export type imgSrcType = {
  url: string;
} & imgTypeSizeDefinitionType;

export type imgSrcTypeProps = {
  viewMode: string;
  styleURIs: ImageStyleURI[];
  customSrcSetTypes?: imgTypeSizeDefinitionType[];
};

export const imgSrcSetTypes: imgTypeSizeDefinitionType[] = [
  { type: "squared_extra_large", size: "1440w", media: "(max-width: 1439px)" },
  { type: "squared_large", size: "1024w", media: "(max-width: 1023px)" },
  { type: "squared_medium", size: "768w", media: "(max-width: 719px)" },
  { type: "squared_small", size: "480w", media: "(max-width: 479px)" },
  { type: "squared_placeholder", size: "10w", media: "(max-width: 10px)" },

  { type: "hero_extra_large", size: "1440w", media: "(max-width: 1439px)" },
  { type: "hero_large", size: "1024w", media: "(max-width: 1023px)" },
  { type: "hero_medium", size: "768w", media: "(max-width: 719px)" },
  { type: "hero_small", size: "480w", media: "(max-width: 479px)" },
  { type: "hero_placeholder", size: "10w", media: "(max-width: 10px)" },

  { type: "landscape_extra_large", size: "1440w", media: "(max-width: 1439px)" },
  { type: "landscape_large", size: "1024w", media: "(max-width: 1023px)" },
  { type: "landscape_medium", size: "768w", media: "(max-width: 719px)" },
  { type: "landscape_small", size: "480w", media: "(max-width: 479px)" },
  { type: "landscape_placeholder", size: "10w", media: "(max-width: 10px)" },

  { type: "banner_extra_large", size: "1440w", media: "(max-width: 1439px)" },
  { type: "banner_large", size: "1024w", media: "(max-width: 1023px)" },
  { type: "banner_medium", size: "768w", media: "(max-width: 719px)" },
  { type: "banner_small", size: "480w", media: "(max-width: 479px)" },
  { type: "banner_placeholder", size: "10w", media: "(max-width: 10px)" },

  { type: "original_extra_large", size: "1440w", media: "(max-width: 1439px)" },
  { type: "original_large", size: "1024w", media: "(max-width: 1023px)" },
  { type: "original_medium", size: "768w", media: "(max-width: 719px)" },
  { type: "original_small", size: "480w", media: "(max-width: 479px)" },
  { type: "original_placeholder", size: "10w", media: "(max-width: 10px)" },

  { type: "portrait_extra_large", size: "1440w", media: "(max-width: 1439px)" },
  { type: "portrait_large", size: "1024w", media: "(max-width: 1023px)" },
  { type: "portrait_medium", size: "768w", media: "(max-width: 719px)" },
  { type: "portrait_small", size: "480w", media: "(max-width: 479px)" },
  { type: "portrait_placeholder", size: "10w", media: "(max-width: 10px)" },
];

export const getImgSourcesByTypes = (props: imgSrcTypeProps): imgSrcType[] => {
  const { viewMode, styleURIs, customSrcSetTypes } = props;
  const styleObj = Object.assign({}, ...styleURIs);
  const srcSetTypes = customSrcSetTypes ? customSrcSetTypes : imgSrcSetTypes;
  let result: imgSrcType[] = [];

  const viewModeTypes = _.filter(
    srcSetTypes,
    (obj) => _.first(_.split(obj.type, "_", 1)) === viewMode
  );

  result = viewModeTypes.map((item, index) => {
    return {
      url: styleObj[item.type],
      size: item.size,
      media: item.media,
      type: item.type,
    };
  });

  return result;
};
