import URI from "urijs";

export const UrlBuilder = (function () {
  return {
    /**
     *
     * @param {
     *  {
     *      baseUrl: string,
     *      bundle: string,
     *      type: string,
     *      guid: string,
     *      includes?: {includes: { referenceField: string, nestedFields?: string[] }[] },
     *      fields?: {type: string, values: string[]}[],
     *      sort?: {sort: {referenceField: string, nestedFields?: string[]}[]}
     *
     *  }} props
     * @returns
     */
    constructUrl(props) {
      return _constructUrl(props);
    },
    /**
     *
     * @param { {includes: { referenceField: string, nestedFields?: string[] }[] }} props
     * @returns
     */
    getIncludesString(props) {
      return _getIncludes(props);
    },
    /**
     *
     * @param {{type: string, values: string[]}[]} props
     * @returns
     */
    getFieldsString(props) {
      return _getFields(props);
    },
    /**
     *
     * @param {{sort: { referenceField: string, nestedFields?: string[] }[] }} props
     * @returns
     */
    getSortString(props) {
      return _getSorted(props);
    },
  };
})();

/* export const getBaseUrl = () => {
  return new URI({ hostname: apiConfig.apiBase, protocol: "https" }).valueOf();
}; */

function _constructUrl({
  baseUrl,
  bundle,
  type,
  guid = "",
  includes,
  fields,
  sort,
}) {

  let uriWithPaths = new URI(baseUrl);

  if (bundle) {
    uriWithPaths.segment(bundle);
  }
  if (type) {
    uriWithPaths.segment(type);
  }
  if (guid) {
    uriWithPaths.addQuery(`filter[id]`, guid.toString());
  }

  /**
   * includes
   */
  let uriWithIncludes = uriWithPaths.clone();

  if (includes !== undefined && includes.length > 0) {
    uriWithIncludes.addQuery(`include`, _getIncludes(includes));
  }
  /**
   * fields
   */
  let uriWithQuery = uriWithIncludes.clone();

  if (fields !== undefined && fields.length > 0) {
    fields.forEach((field) => {
      uriWithQuery.addQuery(`fields[${field.type}]`, field.values.toString());
    });
  }

  /**
   * sort
   */
  let uriSorted = uriWithQuery.clone();

  if (sort !== undefined && sort.length > 0) {
    uriSorted.addQuery(`sort`, _getSorted(sort));
  }

  return decodeURI(uriSorted.valueOf().replace(/%2C/g, ","));
}

/**
 *
 * @param {{includes: { referenceField: string, nestedFields?: string[] }[] }} props
 * @returns
 */
function _getIncludes({ includes }) {
  const result = includes
    .map((include) => {
      let result = [];

      result.push(include.referenceField);

      include.nestedFields?.forEach((nested) => {
        result.push(include.referenceField + "." + nested);
      });
      return result;
    })
    .join(",");

  return result;
}

/**
 *
 * @param {{fields: {type: string, values: string[]}[]}} props
 */
function _getFields({ fields }) {
  const mapped = fields.map((item) => ({
    [item.type]: item.values.toString(),
  }));

  return Object.assign({}, ...mapped);
}

/**
 *
 * @param {{sort: { referenceField: string, nestedFields?: string[] }[] }} props
 * @returns
 */
function _getSorted({ sort }) {
  return sort
    .map((sort) => {
      let result = [];

      result.push(sort.referenceField);

      sort.nestedFields?.forEach((nested) => {
        result.push(sort.referenceField + "." + nested.referenceField);
      });
      return result;
    })
    .join(",");
}
