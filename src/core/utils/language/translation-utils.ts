import _ from "lodash";

export const TranslationUtils = (() => {
  return {
    pickTranslationByLocale(
      locale: string,
      translations: string[],
      locales: string[] = ["is", "en"]
    ) {
      return _pickTranslationByLocale(locale, translations, locales);
    },
  };
})();





function _pickTranslationByLocale(
  locale: string,
  translations: string[],
  locales: string[] = ["is", "en"]
) {
    const translation: string = translations[_.indexOf(locales,locale)];
  return translation;
}
