import { createContext, useContext, useState } from "react";

const GlobalAppContext = createContext(null);

export type GlobalAppContextType = {
  /* isMenuOpen: boolean;
  setIsMenuOpen: (isMenuOpen: boolean) => void;

  doNavAnimate: boolean;
  setNavAnimate: (doNavAnimate: boolean) => void;
  doNavAnimateDown: boolean;
  setNavAnimateDown: (doNavAnimateDown: boolean) => void;

  scrollTopValue: number;
  setScrollTopValue: (value: number) => void;*/
  menuItems: any[];
  setMenuItems: (value: any[]) => void;
};

export function GlobalStateProvider({ children }) {
  /* const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [doNavAnimate, setNavAnimate] = useState(false);
  const [doNavAnimateDown, setNavAnimateDown] = useState(false);
  const [scrollTopValue, setScrollTopValue] = useState(0);*/
  const [menuItems, setMenuItems] = useState([])

  let sharedState: GlobalAppContextType = {
    /* isMenuOpen,
    setIsMenuOpen,
    doNavAnimate,
    setNavAnimate,
    doNavAnimateDown,
    setNavAnimateDown,
    scrollTopValue,
    setScrollTopValue,*/
    menuItems,
    setMenuItems
  };

  return (
    <GlobalAppContext.Provider value={sharedState}>
      {children}
    </GlobalAppContext.Provider>
  );
}

export function useGlobalAppContext() {
  return useContext(GlobalAppContext);
}
