import { createContext, useContext, useState } from "react";

const NavbarContext = createContext(null);

export type NavbarContextType = {
  isMenuOpen: boolean;
  setIsMenuOpen: (isMenuOpen: boolean) => void;

  doNavAnimate: boolean;
  setNavAnimate: (doNavAnimate: boolean) => void;
  doNavAnimateDown: boolean;
  setNavAnimateDown: (doNavAnimateDown: boolean) => void;

  scrollTopValue: number;
  setScrollTopValue: (value: number) => void;

/*   menuItems: any[];
  setMenuItems: (value: any[]) => void; */
};

export function NavbarStateProvider({ children }) {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [doNavAnimate, setNavAnimate] = useState(false);
  const [doNavAnimateDown, setNavAnimateDown] = useState(false);
  const [scrollTopValue, setScrollTopValue] = useState(0);
  /* const [menuItems, setMenuItems] = useState([]) */

  let sharedState: NavbarContextType = {
    isMenuOpen,
    setIsMenuOpen,
    doNavAnimate,
    setNavAnimate,
    doNavAnimateDown,
    setNavAnimateDown,
    scrollTopValue,
    setScrollTopValue,
    /* menuItems,
    setMenuItems */
  };

  return (
    <NavbarContext.Provider value={sharedState}>
      {children}
    </NavbarContext.Provider>
  );
}

export function useNavbarContext() {
  return useContext(NavbarContext);
}
