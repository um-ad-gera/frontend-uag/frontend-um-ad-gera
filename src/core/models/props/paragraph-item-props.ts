export type ParagraphItemProps = {
  data: any;
  mode?:
    | "Basic"
    | "Page"
    | "FrontPage"
    | "EmployeeTeaser"
    | "ProjectSingle"
    | "ProjectMultiple";
};