import React from "react";
import styled, { css } from "styled-components";
import CardDescription, {
  CardDescriptionType,
} from "../title-description-card/card-description";
import CardImage, {
  ImageProps,
} from "../title-description-card/card-image";
import { CardNumberStyle } from "../title-description-card/card-number";
import CardTitle, {
  SplitTitleType,
} from "../title-description-card/card-title";

type ContentHeaderProps = {
  image: ImageProps;
  title: SplitTitleType;
  dark?: boolean;
} & CardDescriptionType;

const StyledContentHeader = styled.div<{
  number?: string;
}>`
  display: block;
  position: relative;
  height: 100vw;
  ${(props) =>
    props.number &&
    css`
      &:before {
        content: "${props.number.padStart(2, "0")}";
        ${CardNumberStyle}
        position: absolute;
        left: -58px;
        top: 7px;
      }
    `}
`;

function ContentHeader(props: ContentHeaderProps) {
  const { image, title, description, dark } = props;
  return (
    <StyledContentHeader>
      <CardImage {...image} />
      <CardTitle title={title} dark={dark} />
      <CardDescription description={description} />
    </StyledContentHeader>
  );
}

export default ContentHeader;
