import { useRouter } from 'next/router';
import React from 'react';
import { TranslationUtils } from '../../../core/utils/language/translation-utils';
import styles from "./content-tiles.module.css";

interface Props {
  alt?: string;
}

type Object = Props &
  React.HTMLProps<HTMLObjectElement> &
  React.HTMLAttributes<HTMLObjectElement>;

  function Object({...props}: Object) {
    return (
      <object {...props}></object>
    )
  }

function ContentTilesOurCustomers() {
  const {locale} = useRouter()
    return (
      <div className={`${styles["OurCustomers"]}`}>
        <h2 className={`${styles["OurCustomers__title"]}`}>
          {TranslationUtils.pickTranslationByLocale(locale, [
            "Viðskiptavinir",
            "Customers",
          ])}
        </h2>
        <div className={`${styles["OurCustomers__wrapper"]}`}>
          <picture>
            <div className={`${styles["OurCustomers__image-wrapper"]}`}>
              <Object
                type="image/svg+xml"
                data="/reykjavik-logo.svg"
                alt="reykjavik-logo"
              ></Object>
            </div>
          </picture>
          <picture>
            <div className={`${styles["OurCustomers__image-wrapper"]}`}>
              <Object
                type="image/svg+xml"
                data="/elding-logo.svg"
                alt="elding-logo"
              ></Object>
            </div>
          </picture>
          <picture>
            <div className={`${styles["OurCustomers__image-wrapper"]}`}>
              <Object
                type="image/svg+xml"
                data="/hafnarfjordur-logo.svg"
                alt="hafnarfjordur-logo"
              ></Object>
            </div>
          </picture>
          <picture>
            <div className={`${styles["OurCustomers__image-wrapper"]}`}>
              <Object
                type="image/svg+xml"
                data="/dicetower-logo.svg"
                alt="dicetower-logo"
              ></Object>
            </div>
          </picture>
          <picture>
            <div className={`${styles["OurCustomers__image-wrapper"]}`}>
              <Object
                type="image/svg+xml"
                data="/arbaejarsafn-logo.svg"
                alt="arbaejarsafn-logo"
              ></Object>
            </div>
          </picture>
          <picture>
            <div className={`${styles["OurCustomers__image-wrapper"]}`}>
              <Object
                type="image/svg+xml"
                data="/arbaejarsafn-logo.svg"
                alt="arbaejarsafn-logo"
              ></Object>
            </div>
          </picture>
          <picture>
            <div className={`${styles["OurCustomers__image-wrapper"]}`}>
              <Object
                type="image/svg+xml"
                data="/hafnarfjordur-logo.svg"
                alt="hafnarfjordur-logo"
              ></Object>
            </div>
          </picture>
          <picture>
            <div className={`${styles["OurCustomers__image-wrapper"]}`}>
              <Object
                type="image/svg+xml"
                data="/dicetower-logo.svg"
                alt="dicetower-logo"
              ></Object>
            </div>
          </picture>
          <picture>
            <div className={`${styles["OurCustomers__image-wrapper"]}`}>
              <Object
                type="image/svg+xml"
                data="/elding-logo.svg"
                alt="elding-logo"
              ></Object>
            </div>
          </picture>
          <picture>
            <div className={`${styles["OurCustomers__image-wrapper"]}`}>
              <Object
                type="image/svg+xml"
                data="/reykjavik-logo.svg"
                alt="reykjavik-logo"
              ></Object>
            </div>
          </picture>
        </div>
      </div>
    );
}

export default ContentTilesOurCustomers
