import React from "react";
import styled, { css } from "styled-components";
import CardDescription, { CardDescriptionType } from "./card-description";
import { CardNumberStyle } from "./card-number";
import CardTitle, { CardTitleType } from "./card-title";

export type TitleDescriptionCardType = {
  number?: string;
} & CardTitleType & CardDescriptionType;

const StyledTitleDescriptionCard = styled.div<{
  hasNumber?: boolean;
  number?: string;
}>`
  display: block;
  position: relative;
  margin-left: 20%;
  width: 40%;
  ${(props) =>
    props.number &&
    css`
      &:before {
        content: "${props.number.padStart(2, "0")}";
        ${CardNumberStyle}
        position: absolute;
        left: -58px;
        top: 7px;
      }
    `}
`;

function TitleDescriptionCard(props: TitleDescriptionCardType) {
  const { number, title, description, dark } = props;
  return (
    <StyledTitleDescriptionCard number={number}>
      <CardTitle title={title} dark={dark} />
      <CardDescription description={description} />
    </StyledTitleDescriptionCard>
  );
}

export default TitleDescriptionCard;
