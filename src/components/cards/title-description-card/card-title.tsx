import React from "react";
import styled from "styled-components";

export type SplitTitleType = {
  first?: string;
  second?: string;
};

export type CardTitleType = {
  title: SplitTitleType;
  dark?: boolean;
};

export const StyledCardTitle = styled.span<{
  isSecond?: boolean;
  dark?: boolean;
}>`
  font-family: "Fontspring-VanSans-ExtraBold";
  font-size: 97px;
  line-height: 99%;
  letter-spacing: -0.03em;
  color: ${(props) =>
    props.isSecond ? (props.dark ? "#1D1D1D" : "#F4F4F4") : "#29ABE2"};
  display: block;
  position: relative;
  bottom: ${(props) => (props.isSecond ? "35px" : "0px")};
`;

function CardTitle(props: CardTitleType) {
  const { title, dark } = props;
  return !title ? (
    <></>
  ) : (
    <h2>
      <StyledCardTitle dark={dark}>{title.first}</StyledCardTitle>
      {!title.second ? (
        <></>
      ) : (
        <StyledCardTitle isSecond={true} dark={dark}>
          {title.second}
        </StyledCardTitle>
      )}
    </h2>
  );
}

export default CardTitle;
