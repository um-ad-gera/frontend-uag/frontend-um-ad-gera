import React, { useEffect, useState } from "react";
import styled from "styled-components";

export type FormattedTextType = {
  format: string;
  processed: string;
  value: string;
};
export type CardDescriptionType = { description?: FormattedTextType | string };

export const StyledCardDescription = styled.div`
  font-family: "Fontspring-VanSans";
  font-size: 18px;
  line-height: 32px;
  letter-spacing: -0.03em;
  color: #a5a5a5;
  position: relative;
  bottom: 15px;
`;

function CardDescription(props: CardDescriptionType) {
  const { description } = props;

  const [descriptionValue, setDescriptionValue] = useState<string>();

  useEffect(() => {
    if (description) {
      if (typeof description === "string") {
        setDescriptionValue(description);
      } else if (typeof description === "object") {
        setDescriptionValue(description?.processed);
      }
    }
  }, [description]);

  return !descriptionValue ? (
    <></>
  ) : (
    <StyledCardDescription
      dangerouslySetInnerHTML={{ __html: descriptionValue }}
    />
  );
}

export default CardDescription;
