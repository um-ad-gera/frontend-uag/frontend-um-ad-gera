import React from "react";
import Image from "next/image";
import ProgressiveImage from "react-progressive-graceful-image";
import defaultPreloadImage from "../../../assets/images/base64/preloads/uag-logo";

export type ImageProps = {
  src: string;
  priority?: boolean;
  width: number;
  height: number;
  heightAuto?: boolean;
  preloadImage?: string;
};

function CardImage(props: ImageProps) {
  const { src, priority, width, height, heightAuto, preloadImage } = props;
  const imageProps = {
    /* [priority ? "priority" : "loading"]: priority ? priority : "lazy", */
    priority: priority ? priority : false,
    /* "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAD8SURBVChTrY09LwRRFIbPx52vlbHJhJAloVIsP4BsJ9GJRqvQ6kVFoaNQKMa2En9AFP6AKGRr5YaWxEcw7szdmXsc/2Hf5pz35H2fA2MXHp1etKIwrCrnAvVpYLhAQNf4pg2IqXjpHh/s3eLhSb7CzBPe+20R6SDiEESGGlrUYqO3OZ37RITP4n1XzQ0T9RHgGgnv1L/pfq/FB8PsjH6PAOUp39mtcCQbQVEPAtusxq+2wK/RAn2XIbVbRoFgFfturO9wUWdU1j0o3DJHTCiQCuKUAMRKBzjrX+H52uZ8/WG34iy59C+/ybQxP4P1mXIpf8yS2cnP/9w4BfAH1/Nar+rSlMUAAAAASUVORK5CYII=", */
  };

  return !src ? (
    <></>
  ) : (
    //todo: add alt text
    <ProgressiveImage
      src={src}
      /* placeholder="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK0AAABUCAMAAADH7EegAAADAFBMVEUdHR0cHBwaHBweHR0aGhoYGxwdHBwjHR0hHBwfHx4eHBwVGxwoHRwgHBwqGxwZGhsYGRolJCQlHBwjIiIrIR0SGRwgICAqKiotHhwmJiYhISEVGRs8IRwyHhw1HRwpJyc2JB1FHxwvHhseGRstLCwmHxwvIhwqHRwbGBsWFhYjIB8TFhszMzIxMTE6JB05IRw5Hhw/IBtBHBs/Pj5cMiB6Mh01IRwYGBg4NzcvLy4/KiBLIxwyIhwjHBwOGBw6OTkwJCErIyFFJRxPLR9GKx+EPR6DOB1NKB1lKhw7GxsiGRtTMSF9Oh93OR87KB9fLB0+JBxVIxxRIhwwGxwtGxw1GBsPFRtOMiFyNR9lJBxZJBwnHBwhGxwKFRuKOh5lLx50MR1QKR1JKB1BJR0lGRw2NDSebyuiXylKLSE1KCBWLh5ZKR1gJht5JxoTFBRjPCWTTiSFSiKMWCBxKxxLHhyYWCZrQSaARCN6QyIoIiJxOyGRRCBtMh9ZLx8sGBoIDxlEQ0OJTieSViaJQyV1QCNTOyOKSiKnTyGMbCBjNSCLdh+Vcx+BZR1UKR1+bxsnFhpISEedZi2fViuMVCqheCZcPiSYaiKTXiGDMxxsLBxcJBxrJBtbHhogFxoVDxhVVVROTUyomkerezZLOC6wcStJNCk4LCk9LiekRCFrOSGNUB+QZh5hHhqecTaXijSOgDCmazCAUCiSYicyKSakbiRcNySWSCObUSKYQCGCUx6KXh2DLhyNMRtUHRtLGBodFRpTPza7fzBYPSuzjyqYYSqajiefgSJDNSCVLhsaEBp2aBktDhlxYBJqVw+/s3puaGRdXFyonjfEkDc9NTOrhzK4czBzRiqDdinDbSmOcyiEXyiTfyCYOh13VBo7FhlqHhg1ERgPERTazpLNw2uIb1idj0yJcUJrV0LMrD98ZT97SS61ZSpmSSdCLSff1aDCtWa+s16belWyoVFaSD2ehDqtmyrYZim+qSiuXChpWCFcEhgvAgj//t/r4sfMvIHgm0m4PHvpAAAQ0klEQVRo3uyXS2sTURTH7z3XmblOZiYyRBjIJORBGkFIk8FkYVIX0SZNm5imqbZJY5o0JdVW8VkrrVVS34qIWrVqdVFwIy2I+AK1Ir5xJyqiooIbv4GPjeLrA7jJA/yt7vLHuf9z7rnoP//5z7+D8d8Dqn4wnYt/H1gWVT/4jyxwDlwrJQZOxLhWAoGxWMfUSHx/VpXjANUEmPJEBIwwrv7C/i4uppDheHDUgi5BBHMw18LUgiyiPOYZJ8/QWsguECIGLAynXy7UgC4l4NNRR0atE3GV64oiYxBcelVebhAaHLjK+4zqXBmwc3Uy9jkBqnyGiU6fhYGgYlpudrX20GqWxVSQ6/kAz+lFUbAtChgEBlUtmBMFs89kMcir7Qo/ur6B1T9iULX2GaY+3qbwIqGWwrL8So/qqFtAGVe1CkMhYe1yF4wSblUDo67V4TBGyLhaV41jFwgf8Jz29VuGdy/csabL9rIlIKsYnDqO4qrrNjAAFZ9+f9frMR9WXx1RrBsvnV9vZDmMoFq2MQx/XTEIeKnPPTnAWevthXPBZPDAVbeRYxXKsQhTFlUQjAB+OnIcAh7JhO1ReNXjCiXaw94Wf1gJ9KxX+0v++jnEAbSyP4gMApFKEoeZdreEIyZRSJrswpZF3f7Erm35Br/lQtC3+E7QM6oUXC5dpScCENbXrmBs6Oat1kTSDBkZOK35uKnn2o6xc7bRfpuvxbC+2aZasN4SAJlHlYMTEQe8pIrN8U4tXZ/kZcFk2mJt8R9Ze+ZlpP94pMOlLF3cYzaMLg831DFAWYIqBRAg7QyRCatfKtRHRjxmwWQVk27lyLKtwwOuA6OXfCeebG2NrFkatklqs+C2UbZCUQCE3V2C1Gs0h/Rg8DqxYFOpKRke8FipVtg13IllT9jWIYzt2Na3tf7+9IZtHVKvylGoRJthmSWEz2TC+2K9rpXD0ogc6+kKmq3JgH/dkeOxxIQROEPGZ5bOd+dnB/xrk8ci8+xhWRfkGRDLr8sT4JONRu++0IC21ef2SPm16YFkSDI9bmr9sOnT4wasSFbricPpmaHBRFvYumFFXYeeSfSqeZ5B5YYgQkCwA3PEeLjNzgbbgvHGaFOnW4u/Xbt9/PPntNhcWLvotDeai09Frfl+aVFfyxpjpK1JZhxqRcJLunSqwqh+pk1ulQaj0Vguq8VPh7zZG18lu9Zv9d58djQ7U2rKzWZd51qiq6i/MdeqIwC07HOXACFcF0uk9rylbfvwjhUjZzb0pWKpidjEiz2z2lzriD+xuy1VCm3cvO7+bEN+cLWk7c2mbCAKALjcmxg2gUKpBF2JbLM2PTUYm5iZmZraPn3pRZ9tfvPZD93RVPdwPLorev/o4PpO9/tnmhZbR5fUORkgPDjKbAtAgGIlvSSeKnR8Wdc0WRyaKoYar/ftPm9sWDD2bFPT5O6jk3s3d572NpXeT6dy72/figjECWaZJQg4VG6ITIQtae2mtzt3s7Tx+NDF4vPild07z3b23h67N96dGirmxi4PNZY2n92bLU1uiNt1MkEcSwARVDbkX88YQqDwaQhEbKOhvpFS8Xnn62KxOH7wTiQ2rWl3h6ZDxW/jF/eefHOlcfBsaiQbuub3gAQCT9pl9rduWaR/XaKICDgpK/IszPfPXD++t/T88vibQx87jLNXtubSt7dHxxofnDp1aP/+N8WJhweu53J+ieF4jogYsQQkEZUXQD+ItfaXpqI4fs73cO697dFCHAi2kYVOCkrD9UPRqEzT8kHZ5qOUqMgejtzUEnoIPde2FpppWgkrA3O0qfRwywoxNZ8gFGqllaU96UVF0IPcnRb+A3cfOOdcLny5n+/3fr6PC1cUiokUUk4ZKxJytx2yGM0W858qZuGNxP3r8mJry8vMlnKj1eSxxEblHM+fLQbeiEh8JxJSDgRY/pkcbAjKjlefj7n3rcxm+/SmauntxuR9XRUx5Q6Tx1Zrddg6zcacJ9FyiiRcAG+A/ACMOIIQwcAsyIvZlxZzyGExv3hfVB2lzd4fa9hb7nB4rDaTtcfutr9wq0MpQgzHBiAeUiQ0KOHlwEUErNVU3mopKG6zfHw2R7Iy+u4mdYz64jGz0W3t7LaN9HZ7+m6e2ZpPEQsc8QlA8PDCZvAJAgKi1p3Vt307Fp0UGcSFyhc+vL6ptqHMYas1dXaP9vb3WD2mNq2CeikyXgMxwhwSDJhPD5gP45ewVKlcJBKvbmwp7VjK6I4GgWR1y4mK8vpyk9Hw1eruH7W7zMYCw6n1HKgw7+XMA2KpcLHFBPMn8FPjjC0SFUjkt34UMYHhp+fNzNdGFF7JMeSVlVkHh2w2e2uP69PXazu3sjDx+onipKA6IBMbQaJpssszKHDTU2tCKYRNm7Mk47oh7VjSvcQ2h2eou3PINTri6mx97I4L/98NgDLIH8AEYxmkYJCpn2gglOVmR+7LC8+oK9zvLjQ+cn3pcTpHPpj7hqqS4lnwaQhx/1wWEnh8MRgjrEqh2gtRkWLAQNeGpZ9PTy4+rDaajjhdH3v7u387W3s6QAQ+siwfVeFDiwkeX4wISePvLqs4/p1N4iQpkcpK/bn6rNyCWsujsX673T7o7O2zVolEMGk1+VUvNLB3icOD45NjBgaqj86jEBCpbYwL0euzSstKr46NDX5p/fBr8Mjurjlr5IRMHTP8ACLTtb/NOt7cXF1UFIxoWEbT8OuHTRdPG/qcb15detbR1fXz5/CpC4qgzYspTHVUcLA4c3FhTVNTdnXJLIoQKJT5ETX33yVkv/38cvnzjTOwSspKJOs5EYvFYTAdkD9BdUvUJdcOqfXZNe3zEAYuaB2sV+foipI/o+cyzPLjLwtACIcgjF3mV7aaWblZ9Q0PivMSYs/oEDCS6YoMeVwclcee0MkQRhhjRIDxyRVzFFiEKPITMlNLnhbX7arOSyw5mokQpwqEEIU2Wg4syDFCeO4EUT6+41sgwYim+oMuphBsqFHfaE57V3kn62a7l4NYmb5RfjhAijDms2gaM9kCOCkgb6lDIGEFr7NA5fKVCgjP1ITkxGvXJpIdwd5yL5FqhxNSD072K4x9VIHwh++ekIkGlAIVyzfmp9U0apA48EDBFmX8lUyghEUsgYO5dVrZBFk8WaN0QER8PClGIhUI1XGxWBO4fcXBiHR9slIcpl1FF+xN3qNURURrwKtMRItyB1bqQ+hUtpkNGxhCACG6mMXACfIjEPzt1dxC46jCOH6+73wzu2c32ZnJ7M5kZpPMJjtJ9ppt3di9mGR3k6gl0RhrjZcaYmsXk1a3iqLGa6DW0odiVCriLYHYxIIvGm+gYq0P6lOLBR/EF0Hw/iQI+uSuUVHEC2r8M8z3Z2Dgx+F/5pyZ+fx+kPu37xu/7eCzh6anMhdfMhFioeS5+5ukwMRdDOVwLBm4+f373rg19DPshkLPfdsjAdQhMdgM8tb/AVYaar9hxv/mM7tH7rzxnLvD4dvvvXUyFEoeuWVXKHzhzvPvDPHkoecPH35rYu+t/f5f9r4N4TXvnpqO1msjv9Comy2cHpnZ3ikQp9UrLo/5h0YDM8pQ95b+m/Z277riSGbm4EVS+IEvX3/t7c/fuybDftv653/ikfvuPhSuOy/bfCGgFLl94vHzv0qisj7ed/ChKS75pgZ27OnsP3Eicqy4987n7w17ch8efvL767vnin74zd3hox/cF30pyTZfwAAhONi7pe3S7Q9PhxMeDJaLUrIriK0n7o/tu7Lr/EOZyPiFu2+Khpquu/iNx19+9I4XkvBr1pDcfyyZvCKBbNMle328ezDi83g6PMDnKpXiiVOeRIc0Gmlv7Y417Rlsva0v3LFlJLZj5ujVVz12wcvPvPjZ0/jrh13yymOBUOxERWGbJWDgQwClXlHUDaKHUFFzwqdMTrWvGFJAQvRJs21+9E20HugdvrzzvMefeOKxdz86/Mp3T0kbnMAwLLe8dFtlTJJGQ5s1sgAgsFnuFrGyB4UXGUNCIWzz7DfRmOQbPh4GRUXPWObBU9F1+WRP184d27p6gzvb9u/vuHBsOonQmFexVi/vGAdfKIzQIsMm/biRMcGJJ3wACUeTZS4jgHAEmDoGnwof6Mp8LSkfmydnw/t67eW0mD/VNzAwfedWf/v+YI/kHwpJQUmWYKi/3+/5VcsXbAIsAAMmAxNEKqn14sFtZS8JpgJphQMdTV75eFIy1z/9IN6yVlnQa2qy7M3sOzo5VN8vrH1VHCuO9/dFegZGJnd7/FxmmytoSANy3VQqVTjb5VOpwpWlHE+Rbo51XhhNJJhT7TlwzsPlUtZG2e851b/b72sLtcUeDq7hyfL9dwQGr+HFmOSX8A+C2vwfsnLbEMCoLlUly3BLCKDqhRpp1U+eSrLCUjWrfnM0cVpK2V8MRsev294XkkKRIEx8Oh9fXxt9c/u+viM7A7uigct6gt4GMsobr2GwcfCNlgREhP8iCAC/GAucMykCqla10+5CvlTLFsx8LZ7GsFnSo+fdm9h2RywUUotHZgMqmmZW39LRefUNrUMjs33H5pqC3kDn0Uh00NPhA/QwQEUA87agT2IS9PQOAPxbWCSuywY3AEgYWjq1/LFKZOhu2qacKrSsQzVh6uirXDE7mlgtx1pga9st/c8H5qysDE07rmtp77p8a8vBQwNzK1/vGTvHG22dXa2UPTsDXubtnezfCz70FUejwy1Szy4JfwoLSv+Am4AZRoEbBgem8zQIYdpC05zSconnRFqA4BTX8mJZr0iZ8VUVlFz0pcnW0XuGX30Hq9nVte6hK1uxdbx7520TaqU8Etvvx8GYPg+MRE8AMk2+cnOktdztkyoCvRAMdk22ZmQfhoYv5RKDBrjU4P87ggYMAAEHgA1DBDkmdK1m5U3GQQC4y5qzkC+Gg80Y19Nmfr2yErm2rfJJGSvNk8PnZSScd5IKuPMng1s7Oi/dEgyWz1pEZqKbM0SYXz2tiXknwWgjudLcHp+nZU/Mv2sAWbvPJxWLAxGUG8z4V2sXcGBAYDNmMWgYzvTUYo5yhWycKE6uKVK6XipEcwKJL7tWKYtqwvL7p3q3HZ8ZHhrtkuZRJUdk9eDeHZn6chFQUckRYygrGxMB46qCyGwClSMZnK8DQ+Q+qXFZ4hFFSYwqLW2j7V2BP1kPGIDaOIHQAGRLJzTqhmu2BnmjUKpRrbqoESk8ngKV8qZpl3Qtn6VsbhH0lru34dc9fqpphuUuLFv+k6vlyNSBOTVxPIE2BwRUz4KsCIPH46YbB4B60IRrEQMVGQAmymtrDiVWoO4riCgZEgf8o1FldbCCINkBIE6Om7UFkCDTzZos7cRVw1wqEYAw7Lxr25xUlcjNqfE4O1M17dXjRtyq5gqLVU5uDVeKufaR3ZOheMFJWUSsOeIJVjSdLFvDhTzEs3kVAIjIIp00AVwD4oqiuIYto864LABofh6Dv/vORjlGP4JpTto1SKkby0gxK0W6S3aVWzbVuJM3F7U05skuxRfTFnMcLiiVNnh6YSlt6GjXzGxJi+sLClHOXBmTME5zSQvOuCkCxkQCBbOJAZDgGjkF3f5xgHQuk2UYgpggs5FDhjbUgckSXNOJ8DewBjCsYzkILol0QePATTLSBd05bTsgOLfMLJC9tMgcbSGumsIyU+l4Kl8ywTDz+QIwSi1lc1xzSwtLlr5Y0nN5x5odRNKyVcsi54zWgGpIJ0NvWI5a2gQygCzOAJhulFIkEKjgGgDMZmRbGgmAOobJfgCfitOx3MXC8QAAAABJRU5ErkJggg==" */
      placeholder={preloadImage ?? defaultPreloadImage}
    >
      {/* <img
        src={src}
        alt=""
        style={{
          width: "100%",
          height: "100%",
          objectFit: "cover",
          objectPosition: "74%",
        }}
      /> */}
      {(src: string, loading: boolean) => (
        <div
          style={{
            opacity: loading ? 0.5 : 1,
            filter: loading ? "blur(100px)" : "blur(0)",
            transition: "all 0.2s linear",
          }}
        >
          {loading ? (
            <>
              <img
                src={preloadImage ?? defaultPreloadImage}
                style={{
                  //opacity: 0.5,
                  //filter: "blur(100px)",
                  width: "100%",
                  height: heightAuto ? "auto" : `${(height / width) * 100}%`,
                  objectFit: "contain",
                }}
              />
            </>
          ) : (
            <Image
              src={src}
              alt="Image alt text"
              width={`100%`}
              height={heightAuto ? "auto" : `${(height / width) * 100}%`}
              objectFit="contain"
              layout="responsive"
              {...imageProps}
            ></Image>
          )}
        </div>
      )}
    </ProgressiveImage>
  );
}

export default CardImage;
