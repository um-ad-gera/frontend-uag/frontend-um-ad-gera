import React from "react";
import styled, { css } from "styled-components";

export const CardNumberStyle = () => css`
  font-family: "Fontspring-VanSans-Italic";
  font-size: 49px;
  line-height: 48px;
  letter-spacing: -0.03em;
  color: #a5a5a5;
`;

export const StyledCardNumber = styled.span`
  ${CardNumberStyle}
`;

function CardNumber(props: { number?: string }) {
  const { number } = props;

  return !number ? <></> : <StyledCardNumber>{number}</StyledCardNumber>;
}

export default CardNumber;
