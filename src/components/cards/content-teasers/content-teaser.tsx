import React from "react";
import EmployeeContentTeaser from "./content-teaser-employee";
import OurWorkHeroContentTeaser from "./content-teaser-our-work-hero";
import ServicesHeroContentTeaser from "./content-teaser-services-hero";

function RenderContentTeaserOfType(props: any) {
  const { type } = props;

  switch (type) {
    case "node--umadgera_is_page":
      //todo: add different teasers for different displays of type
      return <ServicesHeroContentTeaser {...props} />;
    case "node--umadgera_is_page_project":
      return <OurWorkHeroContentTeaser {...props} />;
    case "node--employee":
      return <EmployeeContentTeaser {...props} />;
    default:
      return <></>;
  }
}

function ContentTeaser(props: any) {
  return <RenderContentTeaserOfType {...props} />;
}

export default ContentTeaser;
