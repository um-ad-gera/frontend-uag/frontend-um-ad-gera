import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import _ from "lodash";
import styled, { css } from "styled-components";
import styles from "./content-teasers.module.css";
import { JsonApiData } from "../../../core/services/jsonapi-data-service";
import CardImage, { ImageProps } from "../title-description-card/card-image";
import { CardNumberStyle } from "../title-description-card/card-number";
import { SplitTitleType } from "../title-description-card/card-title";
import LinkButtonOurWorkHero from "../../_atoms/commons/buttons/link-variants/link-button-hero-ourwork";
import PlainImage from "../../_atoms/commons/images/image";
import { TranslationUtils } from "../../../core/utils/language/translation-utils";

type OurWorkHeroContentTeaserProps = {
  field_preview_image: any;
  field_split_headline: string[];
  field_preview_text?: string | object;
  path?: { alias: string };
  index: string;
  reversed?: boolean;
};

function OurWorkHeroContentTeaser(props: OurWorkHeroContentTeaserProps) {
  const {
    field_preview_image,
    field_split_headline,
    field_preview_text,
    path,
    index,
    reversed,
  } = props;

  const [validImage, setValidImage] = useState<ImageProps>();
  const [splitHeadline, setSplitHeadline] = useState<SplitTitleType>();
  const [summary, setSummary] = useState<string>();
  const [link, setLink] = useState<string>();

  const { locale } = useRouter();

  useEffect(() => {
    if (JsonApiData.utils.getImgSrcFromImageField(field_preview_image)) {
      setValidImage({
        src:
          process.env.API_REMOTE_BASE_URL +
          JsonApiData.utils.getImgSrcFromImageField(field_preview_image),
        height: field_preview_image.data.field_height,
        width: field_preview_image.data.field_width,
      });
    }
  }, [field_preview_image]);

  useEffect(() => {
    if (field_split_headline?.length > 0) {
      setSplitHeadline(JsonApiData.utils.toSplitHeadline(field_split_headline));
    }
  }, [field_split_headline]);

  useEffect(() => {
    if (field_preview_text) {
      if (typeof field_preview_text === "string") {
        setSummary(field_preview_text);
      } else if (typeof field_preview_text === "object") {
        setSummary(field_preview_text["processed"]);
      }
    }
  }, [field_preview_text]);

  useEffect(() => {
    if (path?.alias?.length > 0) {
      const cleanUrl = JsonApiData.utils.getUrlWithoutRoot(path.alias);
      if (cleanUrl?.length > 0) {
        setLink(cleanUrl);
      }
    }
  }, [path]);
  return (
    <div
      className={`${styles["OurWorkHero"]} ${
        _.parseInt(index) % 2 ? styles["OurWorkHero--reversed"] : ""
      } ${reversed ? styles["OurWorkHero--reversed"] : ""}`}
    >
      <div className={`${styles["OurWorkHero__content"]}`}>
        <div className={`${styles["OurWorkHero__title-wrapper"]}`}>
          <h2 className={`${styles["OurWorkHero__title"]}`}>
            {TranslationUtils.pickTranslationByLocale(locale, [
              "Verkefni",
              "Project",
            ])}
          </h2>
          {splitHeadline && (
            <>
              {splitHeadline?.first && (
                <h2 className={`${styles["OurWorkHero__title"]}`}>
                  {splitHeadline?.first}
                </h2>
              )}
              {splitHeadline?.second && (
                <h2 className={`${styles["OurWorkHero__title"]}`}>
                  {splitHeadline?.second}
                </h2>
              )}
            </>
          )}
        </div>
        {summary && (
          <div
            className={`${styles["OurWorkHero__summary"]}`}
            dangerouslySetInnerHTML={{ __html: summary }}
          ></div>
        )}
        {link && <LinkButtonOurWorkHero href={link} />}
      </div>
      <picture className={`${styles["OurWorkHero__image"]}`}>
        {validImage && (
          <PlainImage
            field_image={field_preview_image}
            field_view_mode="original"
          />
        )}
      </picture>
      {/*       <div className={`${styles["OurWorkHero__images"]}`}>
        <picture>
          <img src="/our-work3.png" />
        </picture>
        <picture>
          <img src="/our-work2.png" />
        </picture>
        <picture>
          <img src="/our-work4.png" />
        </picture>
      </div> */}
    </div>
  );

  return (
    <div
      className={`${styles["ServicesHero"]} ${
        _.parseInt(index) % 2 ? styles["ServicesHero--reversed"] : ""
      } ${styles["ServicesHero--js"]}`}
    >
      <span className={`${styles["ServicesHero__meta"]}`}></span>
      <picture className={`${styles["ServicesHero__image"]}`}>
        {/* <img src="../../../assets/images/services-hero2.png" /> */}
        {!validImage ? <></> : <CardImage {...validImage} />}
      </picture>
      <div className={`${styles["ServicesHero__content"]}`}>
        <div className={`${styles["ServicesHero__title-wrapper"]}`}>
          <h2 className={`${styles["ServicesHero__title"]}`}>
            {splitHeadline?.first}
          </h2>
          <h2 className={`${styles["ServicesHero__title"]}`}>
            {splitHeadline?.second}
          </h2>
        </div>
        <div
          className={`${styles["ServicesHero__summary"]}`}
          dangerouslySetInnerHTML={{ __html: summary }}
        ></div>
      </div>
    </div>
  );
  //todo: split above code into components like below
  /* return (
    <StyledContentHeader>
      {!validImage ? <></> : <CardImage {...validImage} />}

      <CardTitle title={splitHeadline} />
      <CardDescription description={field_preview_text} />
    </StyledContentHeader>
  ); */
}

export default OurWorkHeroContentTeaser;
