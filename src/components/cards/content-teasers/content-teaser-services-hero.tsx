import React, { useEffect, useRef, useState } from "react";
import _ from "lodash";
import styled, { css } from "styled-components";
import styles from "./content-teasers.module.css";
import { JsonApiData } from "../../../core/services/jsonapi-data-service";
import CardDescription from "../title-description-card/card-description";
import CardImage, { ImageProps } from "../title-description-card/card-image";
import { CardNumberStyle } from "../title-description-card/card-number";
import CardTitle, {
  SplitTitleType,
} from "../title-description-card/card-title";

type ServicesHeroContentTeaserProps = {
  field_preview_image: any;
  field_split_headline: string[];
  field_preview_text?: string | object;
  index: string;
  decorate?: boolean;
  outer?: boolean;
};

function ServicesHeroContentTeaser(props: ServicesHeroContentTeaserProps) {
  const {
    field_preview_image,
    field_split_headline,
    field_preview_text,
    index,
    decorate,
    outer,
  } = props;
  const [validImage, setValidImage] = useState<ImageProps>();
  const [splitHeadline, setSplitHeadline] = useState<SplitTitleType>();
  const [serviceHeroNumber, setServiceHeroNumber] = useState<string>();
  const [summary, setSummary] = useState<string>();

  let heroContentRef = useRef<any>();
  let heroContentMetaRef = useRef<any>();

  useEffect(() => {
    if (heroContentRef?.current && heroContentMetaRef?.current) {
      const ServicesHeroHeight =
        Math.round(heroContentRef.current.clientHeight) / 10 + 4;
      const ServicesHeroMeta = heroContentMetaRef.current;

      ServicesHeroMeta.style.minHeight = ServicesHeroHeight + "rem";
    }
  }, [serviceHeroNumber]);

  useEffect(() => {
    if (JsonApiData.utils.getImgSrcFromImageField(field_preview_image)) {
      setValidImage({
        src:
          process.env.API_REMOTE_BASE_URL +
          JsonApiData.utils.getImgSrcFromImageField(field_preview_image),
        height: field_preview_image.data.field_height,
        width: field_preview_image.data.field_width,
      });
    }
  }, [field_preview_image]);

  useEffect(() => {
    if (field_split_headline?.length > 0) {
      setSplitHeadline(JsonApiData.utils.toSplitHeadline(field_split_headline));
    }
  }, [field_split_headline]);

  useEffect(() => {
    if (field_preview_text) {
      if (typeof field_preview_text === "string") {
        setSummary(field_preview_text);
      } else if (typeof field_preview_text === "object") {
        setSummary(field_preview_text["processed"]);
      }
    }
  }, [field_preview_text]);

  useEffect(() => {
    if (index !== undefined && decorate) {
      setServiceHeroNumber(
        _.padStart((_.parseInt(index) + 1).toString(), 2, "0")
      );
    }
  }, [index, decorate]);

  return (
    <div
      className={`${styles["ServicesHero"]} ${
        _.parseInt(index) % 2 ? styles["ServicesHero--reversed"] : ""
      } ${decorate ? styles["ServicesHero--decorated"] : ""} ${
        outer ? styles["ServicesHero--outer"] : ""
      } ServicesHero--js`}
    >
      {serviceHeroNumber && (
        <span
          ref={heroContentMetaRef}
          className={`${styles["ServicesHero__meta"]}`}
        >
          {serviceHeroNumber}
        </span>
      )}
      <picture className={`${styles["ServicesHero__image"]}`}>
        {/* <img src="../../../assets/images/services-hero2.png" /> */}
        {validImage && <CardImage priority {...validImage} />}
      </picture>
      <div
        ref={heroContentRef}
        className={`${styles["ServicesHero__content"]} ServicesHero__content--js`}
      >
        {splitHeadline && (
          <div className={`${styles["ServicesHero__title-wrapper"]}`}>
            {splitHeadline?.first && (
              <h2 className={`${styles["ServicesHero__title"]}`}>
                {splitHeadline?.first}
              </h2>
            )}
            {splitHeadline?.second && (
              <h2 className={`${styles["ServicesHero__title"]}`}>
                {splitHeadline?.second}
              </h2>
            )}
          </div>
        )}
        {summary && (
          <div
            className={`${styles["ServicesHero__summary"]}`}
            dangerouslySetInnerHTML={{ __html: summary }}
          ></div>
        )}
      </div>
    </div>
  );
  //todo: split above code into components like below
  /* return (
    <StyledContentHeader>
      {!validImage ? <></> : <CardImage {...validImage} />}

      <CardTitle title={splitHeadline} />
      <CardDescription description={field_preview_text} />
    </StyledContentHeader>
  ); */
}

export default ServicesHeroContentTeaser;
