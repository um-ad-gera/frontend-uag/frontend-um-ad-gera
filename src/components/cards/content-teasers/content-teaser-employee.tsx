import React, { useEffect, useState } from "react";
import _ from "lodash";
import styled, { css } from "styled-components";
import styles from "./content-teasers.module.css";
import { JsonApiData } from "../../../core/services/jsonapi-data-service";
import CardImage, { ImageProps } from "../title-description-card/card-image";
import { CardNumberStyle } from "../title-description-card/card-number";
import { SplitTitleType } from "../title-description-card/card-title";
import LinkButtonOurWorkHero from "../../_atoms/commons/buttons/link-variants/link-button-hero-ourwork";
import PlainImage from "../../_atoms/commons/images/image";

type EmployeeContentTeaserProps = {
  title: string;
  field_position: string;
  field_email: string;
  field_preview_image: any;
  field_split_headline: string[];
  field_preview_text?: string | object;
  path?: { alias: string };
  index: string;
  reversed?: boolean;
};

function EmployeeContentTeaser(props: EmployeeContentTeaserProps) {
  const {
    title,
    field_position,
    field_email,
    field_preview_image,
    field_split_headline,
    field_preview_text,
    path,
    index,
    reversed,
  } = props;

  const [validImage, setValidImage] = useState<ImageProps>();
  const [splitHeadline, setSplitHeadline] = useState<SplitTitleType>();
  const [summary, setSummary] = useState<string>();
  const [link, setLink] = useState<string>();

  useEffect(() => {
    if (JsonApiData.utils.getImgSrcFromImageField(field_preview_image)) {
      setValidImage({
        src:
          process.env.API_REMOTE_BASE_URL +
          JsonApiData.utils.getImgSrcFromImageField(field_preview_image),
        height: field_preview_image.data.field_height,
        width: field_preview_image.data.field_width,
      });
    }
  }, [field_preview_image]);

  useEffect(() => {
    if (field_split_headline?.length > 0) {
      setSplitHeadline(JsonApiData.utils.toSplitHeadline(field_split_headline));
    }
  }, [field_split_headline]);

  useEffect(() => {
    if (field_preview_text) {
      if (typeof field_preview_text === "string") {
        setSummary(field_preview_text);
      } else if (typeof field_preview_text === "object") {
        setSummary(field_preview_text["processed"]);
      }
    }
  }, [field_preview_text]);

  useEffect(() => {
    if (path?.alias?.length > 0) {
      const cleanUrl = JsonApiData.utils.getUrlWithoutRoot(path.alias);
      if (cleanUrl?.length > 0) {
        setLink(cleanUrl);
      }
    }
  }, [path]);
  return (
    <div className={`${styles["AboutUs__card"]}`}>
      <picture className={`${styles["AboutUs__card-image"]}`}>
        {!validImage ? (
          <></>
        ) : (
          <>
            <PlainImage
              field_image={field_preview_image}
              field_view_mode="original"

            />
          </>
        )}
      </picture>
      <div className={`${styles["AboutUs__content"]}`}>
        <h2 className={`${styles["AboutUs__title"]}`}>
          <a href={`mailto:${_.first(field_email)}`}>{title}</a>
        </h2>
        <p className={`${styles["AboutUs__job-title"]}`}>{field_position}</p>
      </div>
    </div>
  );
}

export default EmployeeContentTeaser;
