import React, { useEffect } from "react";
import { PlainTextProps } from "../../../_atoms/commons/text/plain-text";
import { PlainTextLongProps } from "../../../_atoms/commons/text/plain-text-long";
import QuoteAuthorImage, {
  QuoteAuthorImageProps,
} from "../../../_atoms/quotes/quote-author-image";
import QuoteAuthorName from "../../../_atoms/quotes/quote-author-name";
import QuoteAuthorTitle from "../../../_atoms/quotes/quote-author-title";
import QuoteText from "../../../_atoms/quotes/quote-text";

export type QuoteCardProps = {
  authorImage: QuoteAuthorImageProps;
  authorText: string;
  authorName: string;
  authorTitle: string;
};

function QuoteCard(props: QuoteCardProps) {
  const { authorImage, authorText, authorName, authorTitle } = props;

  return (
    <div>
      {/* <QuoteAuthorImage {...authorImage} /> */}
      <QuoteText text={authorText} />
      <QuoteAuthorName text={authorName} />
      <QuoteAuthorTitle text={authorTitle} />
    </div>
  );
}

export default QuoteCard;