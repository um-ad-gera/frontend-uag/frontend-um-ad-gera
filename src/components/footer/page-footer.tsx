import React, { useEffect, useState } from "react";
import TextFormatted from "../_atoms/commons/text/text-formatted";
import styles from "./page-footer.module.css";

type FooterBlockDataType = { info: string; body: { processed: string } };

function PageFooter(props: { data: FooterBlockDataType[] }) {
  const { data } = props;

  const [footerDataBlocks, setFooterDataBlocks] = useState<
    FooterBlockDataType[]
  >([]);

  useEffect(() => {
    if (data?.length > 0) {
      setFooterDataBlocks(data);
    }
  }, [data]);

  return (
    <>
      <div className={styles["Footer__wrapper"]}>
        {!footerDataBlocks?.length ? (
          <></>
        ) : (
          <>
            <div className={styles["Footer__group"]}>
              <picture className={styles["Footer__group-image"]}>
                <object
                  type="image/svg+xml"
                  data="/umadgera-logo-colored.svg"
                ></object>
              </picture>
            </div>
            {footerDataBlocks.map((block, index) => {
              return (
                <div key={index} className={styles["Footer__group"]}>
                  <h3 className={styles["Footer__group-title"]}>
                    {block.info}
                  </h3>
                  <div>
                    <TextFormatted
                      className={styles["Footer__group-content"]}
                      field_formatted_text={block.body}
                    />
                  </div>
                </div>
              );
            })}
          </>
        )}
      </div>
    </>
  );
}

export default PageFooter;
