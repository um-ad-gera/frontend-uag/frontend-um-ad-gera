import React, { useEffect } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import styles from "./nav-bar.module.css";
import {
  GlobalAppContextType,
  useGlobalAppContext,
} from "../../../core/context/global-state";
import {
  useNavbarContext,
  NavbarContextType,
} from "../../../core/context/navbar-state";

function NavBar(props: { contactUsTitle: string }) {
  const globalContext: GlobalAppContextType = useGlobalAppContext();
  const navbarContext: NavbarContextType = useNavbarContext();
  const { locale, defaultLocale } = useRouter();

  const { contactUsTitle } = props;

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 100) {
        navbarContext.setNavAnimate(true);

        setTimeout(function () {
          navbarContext.setNavAnimateDown(true);
        }, 1000);
      } else {
        navbarContext.setNavAnimate(false);

        setTimeout(function () {
          navbarContext.setNavAnimateDown(false);
        }, 1000);
      }
    };
    handleScroll();

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <nav
      className={`${styles["Navigation"]} ${styles["Navigation--fixed"]} ${
        navbarContext.doNavAnimate ? styles["Navigation--animate"] : ""
      } ${
        navbarContext.doNavAnimateDown ? styles["Navigation--animate-down"] : ""
      } ${navbarContext.isMenuOpen ? styles["mobile-menu-active"] : ""}`}
    >
      <div
        className={`${styles["Navigation__wrapper"]} ${
          navbarContext.doNavAnimate
            ? styles["Navigation__wrapper--animate"]
            : ""
        }`}
      >
        <Link href="/">
          <a>
            <object
              className={`${styles["Navigation__logo-object"]}`}
              type="image/svg+xml"
              data="/umadgera-logo-colored.svg"
            ></object>
          </a>
        </Link>
        <div
          className={`${styles["burger"]}`}
          onClick={() => navbarContext.setIsMenuOpen(!navbarContext.isMenuOpen)}
        >
          <object type="image/svg+xml" data="/burger.svg"></object>
          <object type="image/svg+xml" data="/burger-active.svg"></object>
        </div>
        <ul className={`${styles["Navigation__menu"]}`}>
          {!(globalContext?.menuItems?.length > 0) ? (
            <></>
          ) : (
            <>
              {globalContext.menuItems.map((item, index) => {
                return (
                  <li
                    className={`${
                      globalContext.menuItems.length === index + 1
                        ? "" /* styles["Navigaton__focused"] */
                        : ""
                    }`}
                    key={index}
                  >
                    <Link href={item.url}>
                      <a onClick={() => navbarContext.setIsMenuOpen(false)}>
                        {item.title}
                      </a>
                    </Link>
                  </li>
                );
              })}
              <li className={styles["Navigaton__focused"]}>
                <a
                  href="#"
                  onClick={(e) => {
                    e.preventDefault();
                    navbarContext.setIsMenuOpen(false);
                    /* window.scrollTo(0,document.body.scrollHeight) */
                    window.scrollTo({
                      behavior: "smooth",
                      top: document.body.scrollHeight,
                    });
                  }}
                >
                  {contactUsTitle}
                </a>
              </li>
              <li className={styles["language-switch"]}>
                <Link href="/" locale="en">
                  <a
                    className={locale === "en" ? styles["active"] : ""}
                    href="/"
                  >
                    EN
                  </a>
                </Link>
                <Link href="/" locale="is">
                  <a
                    className={locale === defaultLocale ? styles["active"] : ""}
                    href="/"
                  >
                    IS
                  </a>
                </Link>
              </li>
            </>
          )}
        </ul>
      </div>
    </nav>
  );
}

export default NavBar;
