import React from "react";
import Image from "next/image";
import bg from '../../assets/images/main-hero-bg-min.png';
import { JsonApiData } from "../../core/services/jsonapi-data-service";
import ContentHeader from "../cards/content-headers/content-header";
import styles from "./page-header.module.css";

type PageHeaderProps = {
  field_header_headline: string[];
  field_header_image: any;
  field_header_text: string;
};

function PageHeader(props: PageHeaderProps) {
  const { field_header_headline, field_header_image, field_header_text } =
    props;

    return (
      <div className={styles["MainHero"]}>
        <div className={styles["MainHero__content"]}>
          <div className={styles["MainHero__title-wrapper"]}>
            <h2 className={styles["MainHero__title"]}>
              {field_header_headline[0]}
            </h2>
            <h2 className={styles["MainHero__title"]}>
              {field_header_headline[1]}
            </h2>
          </div>
          <div className={styles["MainHero__summary"]}>
            {field_header_text}
          </div>
        </div>
      </div>
    );
}

export default PageHeader;
