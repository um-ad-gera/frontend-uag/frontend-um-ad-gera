import React from "react";
import styled, { css } from "styled-components";

type PlainTextStyleProps = {
  invert?: boolean;
  styleOverride?: { style: any };
};
export type PlainTextProps = { text: string } & PlainTextStyleProps;

export const PlainTextStyle = () => css`
  font-family: "Fontspring-VanSans";
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 32px;
  color: #29abe2;
`;

const StyledPlainText = styled.div<PlainTextStyleProps>`
  ${(props) =>
    props?.styleOverride
      ? css`
          ${props.styleOverride.style}
        `
      : css`
          ${PlainTextStyle}
        `}
`;

function PlainText(props: PlainTextProps) {
  const { text } = props;

  return !text ? <></> : <StyledPlainText {...props}>{text}</StyledPlainText>;
}

export default PlainText;
