import React from "react";
import styled, { css } from "styled-components";

type PlainTextLongStyleProps = {
  invert?: boolean;
  styleOverride?: { style: any };
};
export type PlainTextLongProps = { text: string } & PlainTextLongStyleProps;

export const PlainTextLongStyle = () => css`
  font-family: "Fontspring-VanSans";
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 32px;
  color: #fcfcfc;
`;

const StyledPlainTextLong = styled.div<PlainTextLongStyleProps>`
  ${(props) =>
    props.styleOverride
      ? css`
          ${props.styleOverride.style}
        `
      : css`
          ${PlainTextLongStyle}
        `}
`;

function PlainTextLong(props: PlainTextLongProps) {
  const { text } = props;
  return !text ? (
    <></>
  ) : (
    <StyledPlainTextLong {...props}>{text}</StyledPlainTextLong>
  );
}

export default PlainTextLong;
