import React from "react";

function TextFormatted(props: { field_formatted_text: { processed: string }, className?: string }) {
  const { field_formatted_text, className } = props;
  return (
    <div className={className}
      dangerouslySetInnerHTML={{ __html: field_formatted_text?.processed }}
    />
  );
}

export default TextFormatted;
