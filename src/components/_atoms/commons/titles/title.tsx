import React, { useEffect, useState } from "react";

function Title(props: { field_title: string }) {
  const { field_title } = props;

  const [title, setTitle] = useState<string>();

  useEffect(() => {
    if (field_title && field_title.length > 0) {
      setTitle(field_title);
    }
    return () => {};
  }, [field_title]);

  return !title ? <></> : <h2 dangerouslySetInnerHTML={{ __html: title }} />;
}

export default Title;
