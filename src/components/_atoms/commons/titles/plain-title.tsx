import React from "react";

export type SplitTitleType = {
  first?: string;
  second?: string;
};

type PlainTitleType = {
  title: SplitTitleType;
  dark?: boolean;
};

function PlainTitle(props: PlainTitleType) {
  const { title, dark } = props;
  return !title ? (
    <></>
  ) : (
    <h2>
      <span>{title.first}</span>
      {!title.second ? <></> : <span>{title.second}</span>}
    </h2>
  );
}

export default PlainTitle;
