import React, { useEffect, useState } from "react";
import {
  getImgSourcesByTypes,
  imgSrcType,
} from "../../../../core/utils/img/image-styles";
import _ from "lodash";
import CardImage from "../../../cards/title-description-card/card-image";

export type ImageProps = {
  priority?: boolean;
  width: number;
  height: number;
};

function PlainImage(props: { field_image: any, field_view_mode: string, heightAuto?: boolean }) {
  const { field_image, field_view_mode, heightAuto } = props;

  const [validImage, setValidImage] = useState<ImageProps>();
  const [imgSrces, setImgSrces] = useState<imgSrcType[]>([]);

  useEffect(() => {
    if (field_image?.data?.field_media_image?.data?.image_style_uri) {
      setImgSrces(
        getImgSourcesByTypes({
          viewMode: field_view_mode,
          styleURIs: field_image.data.field_media_image.data.image_style_uri,
        })
      );
    }
  }, [field_image]);

  useEffect(() => {
    if (field_image?.data?.field_height && field_image?.data?.field_width) {
      setValidImage({
        height: field_image.data.field_height,
        width: field_image.data.field_width,
      });
    }
  }, [props]);

  const imageProps = {
    /* [priority ? "priority" : "loading"]: priority ? priority : "lazy", */
  };

  return !(imgSrces.length > 0) || !validImage ? (
    <></>
  ) : (
    <>
      <CardImage
        priority
        src={_.first(imgSrces)?.url!}
        width={validImage.width}
        height={validImage.height}
        heightAuto={heightAuto}
        preloadImage={_.last(imgSrces)?.url!}
      />
    </>
  );
}

export default PlainImage;
