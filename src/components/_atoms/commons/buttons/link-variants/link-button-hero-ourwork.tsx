import Link, { LinkProps } from "next/link";
import React from "react";
import Button from "../simple/button";
import styles from "./link-button-variants.module.css"

function LinkButtonOurWorkHero(props: LinkProps) {
  return (
    <div className={`${styles["OurWorkHero__button"]}`}>
      <Link passHref {...props}>
        <a>
          <Button />
        </a>
      </Link>
    </div>
  );
}

export default LinkButtonOurWorkHero;
