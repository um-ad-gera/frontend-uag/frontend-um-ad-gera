import Link, { LinkProps } from "next/link";
import React from "react";
import Button from "../simple/button";

function LinkButtonPrimary(props: {
  buttonProps?: { buttonText: string };
  linkProps: LinkProps;
}) {
  const { linkProps, buttonProps } = props;

  return (
    <div>
      <Link passHref {...linkProps}>
        <a>
          <Button {...buttonProps} />
        </a>
      </Link>
    </div>
  );
}

export default LinkButtonPrimary;
