import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { TranslationUtils } from "../../../../../core/utils/language/translation-utils";
import styles from "./button.module.css";

function Button(props: { buttonText?: string }) {
  const { buttonText } = props;

  const [buttonState, setButtonState] = useState<string>();

  const {locale} = useRouter();

  useEffect(() => {
    if (buttonText) {
      setButtonState(buttonText);
    }
  }, [props]);



  return (
    <div className={`${styles["ButtonPrimary"]}`}>
      <span className={`${styles["ButtonPrimary__title"]}`}>
        {buttonState
          ? buttonState
          : TranslationUtils.pickTranslationByLocale(locale, [
              "Lesa meira",
              "Read more",
            ])}
      </span>
      <span className={`${styles["ButtonPrimary__icon"]}`}>
        <span className={`${styles["ButtonPrimary__icon"]}`}>
          <svg>
            <use xlinkHref="/sprites/svg-sprites.svg#circle-button"></use>
          </svg>
        </span>
      </span>
    </div>
  );
}

export default Button;
