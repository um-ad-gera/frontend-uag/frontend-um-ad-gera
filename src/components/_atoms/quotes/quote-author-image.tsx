import React from "react";
import Image from "next/image";
import styled, { css } from "styled-components";

export type QuoteAuthorImageProps = {
  src: string;
  priority?: boolean;
  altText?: string;
};

export const QuoteAuthorImageStyle = () => css``;

const StyledQuoteAuthorImage = styled(Image)`
  ${QuoteAuthorImageStyle}
`;

function QuoteAuthorImage(props: QuoteAuthorImageProps) {
  const { src, priority, altText } = props;
  const imageProps = {
    alt: altText ?? "Picture of the author",
    [priority ? "priority" : "loading"]: priority ? priority : "lazy",
    layout: "fill",
  };
  return !src ? (
    <></>
  ) : (
    <StyledQuoteAuthorImage
      src={src}
      objectFit="cover"
      {...imageProps}
    ></StyledQuoteAuthorImage>
  );
}

export default QuoteAuthorImage;



