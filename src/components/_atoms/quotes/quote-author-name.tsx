import React from "react";
import { css } from "styled-components";
import PlainText, {
  PlainTextProps,
  PlainTextStyle,
} from "../../_atoms/commons/text/plain-text";

const QuoteAuthorNameStyle = () => css`
  ${PlainTextStyle}
  color: #333;
`;

function QuoteAuthorName(props: PlainTextProps) {
  return (
    <PlainText
      styleOverride={{ style: QuoteAuthorNameStyle }}
      {...props}
    ></PlainText>
  );
}

export default QuoteAuthorName;
