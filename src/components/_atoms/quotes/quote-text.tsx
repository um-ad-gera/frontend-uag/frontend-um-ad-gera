import React from "react";
import PlainTextLong from "../../_atoms/commons/text/plain-text-long";

function QuoteText(props: { text: string }) {
  const { text } = props;
  return <PlainTextLong text={text} />;
}

export default QuoteText;

/*

import React, { useEffect, useState } from "react";
import styled, { css } from "styled-components";
import PlainTextLong, {
  PlainTextLongProps,
  PlainTextLongStyle,
} from "../../_atoms/commons/text/plain-text-long";

const QuoteTextStyle = () => css`
  ${PlainTextLongStyle}
  color: #333;
`;

const StyledQuoteText = styled(PlainTextLong)<PlainTextLongProps>`
  ${(props) =>
    props?.styleOverride
      ? css`
          ${props.styleOverride.style}
        `
      : css`
          ${QuoteTextStyle}
        `}
`;

function QuoteText(props: PlainTextLongProps) {
  const { styleOverride } = props;

  const [newStyle, setNewStyle] = useState(undefined);

  useEffect(() => {
    if (styleOverride) {
      setNewStyle(styleOverride);
    }
  }, [styleOverride]);

  styleOverride.style = QuoteTextStyle;
  return (
    <StyledQuoteText styleOverride={newStyle} {...props}></StyledQuoteText>
  );
}

export default QuoteText;
 */
