import React from "react";
import { css, FlattenInterpolation, ThemeProps } from "styled-components";
import PlainText, {
  PlainTextProps,
  PlainTextStyle,
} from "../../_atoms/commons/text/plain-text";

const QuoteAuthorTitleStyle = () => css`
  ${PlainTextStyle}
  color: #ffffff;
`;

function QuoteAuthorTitle(props: PlainTextProps) {
  return (
    <PlainText
      styleOverride={{ style: QuoteAuthorTitleStyle }}
      {...props}
    ></PlainText>
  );
}

export default QuoteAuthorTitle;
