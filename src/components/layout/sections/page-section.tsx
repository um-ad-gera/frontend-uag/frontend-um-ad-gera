import React, { createElement } from "react";
import styles from "./page-section.module.css";

type AllPageSectionProps = {
  children: JSX.Element;
  dark?: boolean;
  bottomSeparator?: boolean;
  topSeparator?: boolean;
  noTopPadding?: boolean;
  footer?: boolean;
  mainHero?: boolean;
  container?: boolean;
};

//Todo: footer or mainHero
type PageSectionProps = AllPageSectionProps;

function PageSection(props: PageSectionProps) {
  const { children, container, dark, bottomSeparator, topSeparator, noTopPadding, footer, mainHero } =
    props;

  const attributes: JSX.ElementAttributesProperty = {
    props: {
      className: `
      ${styles["Section"]
      } ${container ? styles["Section--container"] : ""
      } ${dark ? styles["Section--dark"] : ""
      } ${noTopPadding ? styles["Section--padTopZero"] : ""
      } ${mainHero ? styles["MainHero--bg"] : ""
      } ${dark && mainHero ? styles["MainHero-Section--dark"] : ""
      } ${dark && footer ? styles["Footer-Section--dark"] : ""
      } ${bottomSeparator ? styles["Separator--bottom"] : ""
      } ${topSeparator ? styles["Separator--top"] : ""}`,
    },
  };

  return footer ? (
    <footer {...attributes.props}>{children}</footer>
  ) : (
    <section {...attributes.props}>{children}</section>
  );
}

export default PageSection;
