import React from "react";
import pageStyles from "./paragraph-page.module.css";
import imageStyles from "./paragraph-mynd.module.css";
import Title from "../_atoms/commons/titles/title";
import PlainImage from "../_atoms/commons/images/image";
import { ParagraphItemProps } from "../../core/models/props/paragraph-item-props";

export default function ParagraphMynd(props: ParagraphItemProps) {
  const { data, mode } = props;

  if (!data) {
    return <></>;
  }

  if (!mode || mode === "Basic") {
    return (
      <>
        <Title {...data} />
        {data.field_image.data.map((image, index) => (
          <PlainImage
            key={index}
            field_image={{ data: image }}
            field_view_mode={data.field_view_mode}
            heightAuto
          />
        ))}
      </>
    );
  }
  if (mode === "Page") {
    return (
      <div className={`${pageStyles["Page"]}`}>
        <Title {...data} />
        {data.field_image.data.map((image, index) => (
          <PlainImage
            key={index}
            field_image={{ data: image }}
            field_view_mode={data.field_view_mode}
            heightAuto
          />
        ))}
      </div>
    );
  }
  if (mode === "ProjectSingle") {
    return (
      <>
        <Title {...data} />
        {data.field_image.data.map((image, index) => (
          <PlainImage
            key={index}
            field_image={{ data: image }}
            field_view_mode={data.field_view_mode}
          />
        ))}
      </>
    );
  }
  if (mode === "ProjectMultiple") {
    return (
      <div
        className={`${imageStyles["OurWorkHero__images"]} ${imageStyles["OurWorkInfo__images"]}`}
      >
        <Title {...data} />
        {data.field_image.data.map((image, index) => (
          <PlainImage
            key={index}
            field_image={{ data: image }}
            field_view_mode={data.field_view_mode}
          />
        ))}
      </div>
    );
  }
}
