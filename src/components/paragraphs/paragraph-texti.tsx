import React from "react";
import styles from "./paragraph-page.module.css"
import TextFormatted from "../_atoms/commons/text/text-formatted";
import Title from "../_atoms/commons/titles/title";

export default function ParagraphTexti({ data }) {

  return (
    <div className={`${styles["Page"]}`}>
      <Title {...data} />
      <TextFormatted {...data} />
    </div>
  );
}
