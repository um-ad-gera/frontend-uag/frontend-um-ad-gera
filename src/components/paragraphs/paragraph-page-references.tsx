import React, { useEffect, useState } from "react";
import { ParagraphItemProps } from "../../core/models/props/paragraph-item-props";
import ContentTeaser from "../cards/content-teasers/content-teaser";
import teaserStyles from "./../cards/content-teasers/content-teasers.module.css";

export default function ParagraphPageReferences(
  props: ParagraphItemProps /* props: {
  field_title: any;
  field_t: any;
  field_page_reference: any;
} */
) {
  /* const { field_page_reference } = props; */
  const { data, mode } = props;

  const [pageReferences, setPageReferences] = useState([]);

  useEffect(() => {
    if (data?.field_page_reference?.data) {
      setPageReferences(data.field_page_reference.data);
    }
  }, [data]);

  if (!pageReferences || pageReferences.length === 0) {
    return <></>;
  }

  const TeaserList = (props?: { decorate?: boolean; outer?: boolean }) => {

    return (
      <>
        {pageReferences.map((item, index) => (
          <ContentTeaser key={index} index={index} {...props} {...item} />
        ))}
      </>
    );
  };

  if (mode === "FrontPage") {
    return <>{TeaserList({ decorate: true })}</>;
  }
  if (mode === "EmployeeTeaser") {
    return <div className={`${teaserStyles["AboutUs"]}`}>{TeaserList()}</div>;
  }

  return <>{TeaserList({ outer: true })}</>;
}
