import React from "react";
import ContentTilesOurCustomers from "../cards/content-tiles/content-tiles-our-customers";
import ParagraphPageReferences from "./paragraph-page-references";
import ParagraphTestimonial from "./paragraph-testimonials";
import ParagraphSlideshow from "./paragraph-slideshow";
import ParagraphTexti from "./paragraph-texti";
import ParagraphTextiMedMynd from "./paragraph-texti-med-mynd";
import ParagraphMynd from "./paragraph-mynd";
import { ParagraphItemProps } from "../../core/models/props/paragraph-item-props";

const ParagraphItem = function (props: ParagraphItemProps) {
  const { data, mode } = props;

  function ParagraphOfType(props) {
    switch (props.type) {
      case "paragraph--slideshow":
        return <>{data ? <ParagraphSlideshow data={data} /> : <></>}</>;
      case "paragraph--texti":
        return <>{data ? <ParagraphTexti data={data} /> : <></>}</>;
      case "paragraph--texti_med_mynd":
        return <>{data ? <ParagraphTextiMedMynd data={data} /> : <></>}</>;
      case "paragraph--mynd":
        return <>{data ? <ParagraphMynd mode={mode} data={data} /> : <></>}</>;
      case "paragraph--page_reference":
        return <>{data ? <ParagraphPageReferences mode={mode} data={data} /> : <></>}</>;
      case "paragraph--quote":
        return <>{data ? <ParagraphTestimonial {...data} /> : <></>}</>;
      case "paragraph--gallery":
        return <>{data ? <ContentTilesOurCustomers {...data} /> : <></>}</>;

      default:
        return <></>;
    }
  }

  return <>{!data ? <></> : <ParagraphOfType type={data.type} />}</>;
};
export default ParagraphItem;
