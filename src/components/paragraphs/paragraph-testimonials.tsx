import React, { useEffect, useState } from "react";
import styles from "./paragraph-testimonials.module.css"
import { JsonApiData } from "../../core/services/jsonapi-data-service";
import { QuoteAuthorImageProps } from "../_atoms/quotes/quote-author-image";
import QuoteCard, {
  QuoteCardProps,
} from "../_molecules/cards/quotes/quote-card";
import CardImage, { ImageProps } from "../cards/title-description-card/card-image";

function ParagraphTestimonial(props: {
  field_image: any;
  field_quote_text: string;
  field_author: string;
  field_title: string;
}) {
  const { field_image, field_quote_text, field_author, field_title } = props;

  const [quoteCardData, setQuoteCardData] = useState<QuoteCardProps>();
  const [validImage, setValidImage] = useState<ImageProps>();

useEffect(() => {
  if (JsonApiData.utils.getImgSrcFromImageField(field_image)) {
    setValidImage({
      src:
        process.env.API_REMOTE_BASE_URL +
        JsonApiData.utils.getImgSrcFromImageField(field_image),
      height: field_image.data.field_height,
      width: field_image.data.field_width,
    });
  }
}, [field_image]);

  useEffect(() => {
    if (props) {
      const quoteCardProps: QuoteCardProps = {
        authorImage: {
          src: `${
            process.env.API_REMOTE_BASE_URL
          }${JsonApiData.utils.getImgSrcFromImageField(field_image)}`,
        },
        authorText: field_quote_text,
        authorName: field_author,
        authorTitle: field_title,
      };
      setQuoteCardData(quoteCardProps);
    }
  }, [props]);

  return (
    <>
      <div className={`${styles["Testimonial"]}`}>
        <div className={`${styles["Testimonial__icon"]}`}>
          <svg>
            <use xlinkHref="/sprites/svg-sprites.svg#quote-left"></use>
          </svg>
        </div>
        <div className={`${styles["Testimonial__content"]}`}>
          <div className={`${styles["Testimonial__summary"]}`}>
            {quoteCardData?.authorText}
          </div>
          <div>
            <h2 className={`${styles["Testimonial__name"]}`}>
              {quoteCardData?.authorName}
            </h2>
            <h3 className={`${styles["Testimonial__title"]}`}>
              {quoteCardData?.authorTitle}
            </h3>
          </div>
        </div>
        <picture className={`${styles["Testimonial__image"]}`}>
          {!validImage ? <></> : <CardImage {...validImage} />}
        </picture>
      </div>
    </>
  );

  return !field_quote_text ? <></> : <QuoteCard {...quoteCardData} />;
}

export default ParagraphTestimonial;
