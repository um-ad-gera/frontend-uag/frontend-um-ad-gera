import React from "react";
import styles from "./paragraph-page.module.css"
import TextFormatted from "../_atoms/commons/text/text-formatted";
import Title from "../_atoms/commons/titles/title";
import PlainImage from "../_atoms/commons/images/image";

export default function ParagraphTextiMedMynd({ data }) {

  return (
    <div className={`${styles["Page"]}`}>
      <Title {...data} />
      <TextFormatted {...data} />
      <PlainImage {...data} />
    </div>
  );
}
