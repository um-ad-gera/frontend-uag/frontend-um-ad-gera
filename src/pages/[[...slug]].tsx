import { GetStaticProps } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import React, { useEffect, useState } from "react";
import PageSection from "../components/layout/sections/page-section";
import PreloadHeaderSection from "../components/layout/sections/preload-header-section";
import UmadgeraPageContainer from "../containers/umadgera-page-container";
import UmadgeraPageProjectContainer from "../containers/umadgera-page-project-container";
import {
  GlobalAppContextType,
  useGlobalAppContext,
} from "../core/context/global-state";
import { JsonApiData } from "../core/services/jsonapi-data-service";
import DesignComponent from "../design/design-component";

function RenderContainerOfType(props: PageProps) {
  const { data } = props;

  switch (data?.type) {
    case "node--umadgera_is_page":
      return <UmadgeraPageContainer {...props} />;
    case "node--umadgera_is_page_project":
      return <UmadgeraPageProjectContainer {...props} />;
    default:
      return <></>;
  }
}

function Page(props: PageProps) {
  const { isFallback } = useRouter();
  const { menuItems } = props;

  const context: GlobalAppContextType = useGlobalAppContext();

  const [pageData, setPageData] = useState<PageProps>();

  useEffect(() => {
    if (props) {
      setPageData(props);
    }
  }, [props]);

  useEffect(() => {
    context.setMenuItems(menuItems);
  }, [menuItems]);

  if (isFallback) {
    return (
      <PageSection>
        <></>
      </PageSection>
    );
  }

  return !pageData ? (
    <PreloadHeaderSection />
  ) : (
    <>
      <Head>
        <title>{(props.data.title !== "Forsíða" && props.data.title !== "Frontpage") ? props.data.title+' |' : ''} Um að gera</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      {process.env.REACT_APP_DESIGN_MODE === "true" && <DesignComponent />}
      {process.env.REACT_APP_DESIGN_MODE !== "true" && (
        <>
          <RenderContainerOfType {...pageData} />
        </>
      )}
    </>
  );
}

export default Page;

export type PageProps = {
  data: any;
  footer: any;
  hasHeader: boolean;
  hasContentParagraphs: boolean;
  paragraphs?: any[];
  menuItems: MenuItem[];
  contactUsTitle: string;
};

type MenuItem = {
  id: string;
  parentId?: string;
  title: string;
  url: string;
  children?: MenuItem[];
};

interface Params extends ParsedUrlQuery {
  slug: string[];
}

// This function gets called at build time on server-side.
// It may be called again, on a serverless function, if
// revalidation is enabled and a new request comes in
export const getStaticProps: GetStaticProps<PageProps, Params> = async ({
  params,
  locale,
  defaultLocale,
}) => {
  let menuItems: MenuItem[] = [];

  menuItems = await JsonApiData.getAllMenuItems(true,locale,defaultLocale);

  const { slug } = params;
  const validSlug = slug?.join("/") ?? "";
  const pageData = await JsonApiData.getPageData(
    validSlug,
    locale,
    defaultLocale
  );

  if (!pageData) {
    //Note: With notFound: true the page will return a 404 even if there was a successfully generated page before.
    //This is meant to support use-cases like user generated content getting removed by its author.
    return {
      notFound: true,
    };
  }

  let entityWithParagraphs: any;
  let paragraphs: any = {};

  if (pageData?.hasContentParagraphs) {
    entityWithParagraphs = await JsonApiData.getEntityParagraphsData(validSlug, locale, defaultLocale);
    paragraphs = entityWithParagraphs?.field_content_paragraphs?.data;
  }

  return {
    props: { ...pageData, paragraphs, menuItems },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every x seconds
    revalidate: 600,
  };
};

// This function gets called at build time on server-side.
// It may be called again, on a serverless function, if
// the path has not been generated.
// { fallback: false } means other routes should 404.
export async function getStaticPaths(context) {
  const { locales, defaultLocale } = context;

  const dynamicPaths = await JsonApiData.getAllRoutes(locales, defaultLocale);
  return { paths: dynamicPaths, fallback: true };
}
