import URI from "urijs";

export default async function handler(req, res) {
  let newUri = new URI(process.env.API_REMOTE_BASE_URL);
  if (req.url.indexOf("/api/") === 0) {
    newUri.segment(req.url.split("/api/")[1]);
  } else {
    newUri.segment(req.url);
  }


  const apidata = await fetch(
    decodeURI(newUri.valueOf().replace(/%3F/g, "?").replace(/%2F/g, "/"))
  );

  res.json(await apidata.json());
}
