// import App from "next/app";
import type { AppProps } from "next/app";
import React from "react";
import "../../styles/globals.scss";
import PageFooter from "../components/footer/page-footer";
import NavBar from "../components/header/navigation/nav-bar";
import PageSection from "../components/layout/sections/page-section";
import { GlobalStateProvider } from "../core/context/global-state";
import { NavbarStateProvider } from "../core/context/navbar-state";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <GlobalStateProvider>
      <NavbarStateProvider>
        <NavBar contactUsTitle={pageProps?.contactUsTitle} />
      </NavbarStateProvider>
      <Component {...pageProps} />
      <PageSection dark footer topSeparator>
        <PageFooter data={pageProps?.footer} />
      </PageSection>
    </GlobalStateProvider>
  );
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext: AppContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);

//   return { ...appProps }
// }

export default MyApp;
